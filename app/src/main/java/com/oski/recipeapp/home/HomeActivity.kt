package com.oski.recipeapp.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.Button
import com.oski.recipeapp.data.Recipe
import com.oski.recipeapp.helpers.RecipeListCardDecoration
import com.oski.recipeapp.helpers.SafeClickListener
import android.view.Menu
import android.view.MenuItem
import com.oski.recipeapp.R
import com.oski.recipeapp.recipeForm.RecipeFormActivity

class HomeActivity : AppCompatActivity(), HomeContract.View {

    private lateinit var presenter: HomeContract.Presenter

    private lateinit var mGetStartedWrapper: View
    private lateinit var mRecipeListWrapper: View
    private lateinit var mGetStartedBtn: Button
    private lateinit var mRecipeListRv: RecyclerView
    private lateinit var mFab: FloatingActionButton

    private var shouldRefreshList: Boolean = false

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        presenter = HomePresenter(this)
        initView()
    }
    override fun onResume() {
        super.onResume()

        if (shouldRefreshList) {
            mRecipeListRv.swapAdapter(RecipeListAdapter(
                presenter.getRecipeList(presenter.getCurrentRecipeTypeSelection())), false)

            shouldRefreshList = false
        }
    }

    override fun onPause() {
        super.onPause()

        shouldRefreshList = true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val recipeList = presenter.getRecipeTypes()

        if (recipeList != null) {
            for (recipe in recipeList) {
                menu.add(recipe)
            }
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        presenter.setCurrentRecipeTypeSelection(item.toString())
        mRecipeListRv.swapAdapter(RecipeListAdapter(presenter.getRecipeList(item.toString())), false)

        return true
    }

    override fun initView() {
        mGetStartedWrapper = findViewById(R.id.home_get_started_wrapper)
        mGetStartedWrapper.visibility = View.VISIBLE

        mRecipeListWrapper = findViewById(R.id.home_recipe_list_layout)
        mRecipeListWrapper.visibility = View.GONE

        mGetStartedBtn = findViewById(R.id.home_get_started_btn)
        mGetStartedBtn.setSafeOnClickListener {
            presenter.checkFirstTime()
        }

        mRecipeListRv = findViewById(R.id.home_recipe_list_rv)

        if (!presenter.getCurrentRecipeTypeSelection().isNullOrEmpty()) {
            presenter.checkFirstTime()
        }

        mFab = findViewById(R.id.home_fab)
        mFab.setSafeOnClickListener {
            val intent = Intent(this, RecipeFormActivity::class.java)
            startActivity(intent)
        }
    }

    override fun showRecipeList(recipeList: ArrayList<Recipe>) {
        mGetStartedWrapper.visibility = View.GONE

        mRecipeListWrapper.visibility = View.VISIBLE
        mRecipeListRv.setHasFixedSize(true)
        mRecipeListRv.addItemDecoration(RecipeListCardDecoration(this))

        val gridLayoutManager = GridLayoutManager(this, 2)
        mRecipeListRv.layoutManager = gridLayoutManager

        val recipeListAdapter = RecipeListAdapter(presenter.getRecipeList(presenter.getCurrentRecipeTypeSelection()))
        mRecipeListRv.adapter = recipeListAdapter
    }
}
