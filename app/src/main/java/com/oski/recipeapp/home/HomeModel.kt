package com.oski.recipeapp.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.XmlResourceParser
import com.oski.recipeapp.R
import com.oski.recipeapp.data.Recipe
import com.oski.recipeapp.repository.DbContract
import com.oski.recipeapp.repository.RecipesDbHelper
import org.xmlpull.v1.XmlPullParser

class HomeModel (view: HomeContract.View): HomeContract.Model {

    private val context: Context = view as Context
    private val db: DbContract.Recipes = RecipesDbHelper(context)
    private val sharedPref = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE)

    private val xrp: XmlResourceParser = context.resources.getXml(R.xml.recipe_types)
    private var recipeTypes: MutableList<String>? = arrayListOf()

    init {
        if (recipeTypes.isNullOrEmpty()) {
            while (xrp.eventType != XmlPullParser.END_DOCUMENT) {
                if (xrp.eventType == XmlPullParser.START_TAG) {
                    if (xrp.name == "type") {
                        recipeTypes?.add(xrp.getAttributeValue(0))
                    }
                }

                xrp.next()
            }
        }
    }

    @SuppressLint("CommitPrefEdits")
    override fun checkFirstTime(): Boolean {
        var isFirstTime = sharedPref.getBoolean("firstTime", true)

        if (isFirstTime) {
            sharedPref.edit().putBoolean("firstTime", false).apply()

            if (!recipeTypes.isNullOrEmpty()) {
                sharedPref.edit().putString("recipeTypeSelection", recipeTypes!![0]).apply()
            }
        }

        return isFirstTime
    }

    override fun populateDummyData() {
        try {
            val dummyText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu lectus eget purus placerat dignissim non et neque. Ut euismod libero eu ante egestas ultricies. Ut nec dictum metus. Sed lacus nibh, gravida sed blandit sed, aliquet nec sapien. Fusce vulputate nulla nec interdum imperdiet. Duis fringilla quis diam in tempus. Sed pretium luctus mauris nec molestie. Praesent tristique lobortis nisi convallis cursus. Sed non aliquam neque. Suspendisse ut libero facilisis turpis consectetur semper in ac augue. Quisque tincidunt eros tellus, non mollis risus luctus at. Nunc eu posuere augue, eu faucibus nibh. Duis eleifend placerat velit sit amet fermentum. Quisque eu libero at est sodales pellentesque. Nulla pharetra euismod leo, eu blandit dolor pharetra nec. "
            val dummyTime = "30 minutes"

            if (!recipeTypes.isNullOrEmpty()) {
                val iterator = recipeTypes!!.listIterator()
                for (recipeType in iterator) {
                    db.insertRecipe(Recipe(null,"Dummy $recipeType Recipe", recipeType, "", dummyText, dummyTime, dummyText))
                }
            }
        } catch (err: Exception) {
            println(err)
        }
    }

    override fun getRecipeList(recipeType: String?): ArrayList<Recipe> {
        return db.retrieveAllRecipes(recipeType)
    }

    override fun getCurrentRecipeTypeSelection(): String? {
        return sharedPref.getString("recipeTypeSelection", "")
    }

    override fun getRecipeTypes(): MutableList<String>? {
        return recipeTypes
    }

    override fun setCurrentRecipeTypeSelection(recipeType: String) {
        sharedPref.edit().putString("recipeTypeSelection", recipeType).apply()
    }
}
