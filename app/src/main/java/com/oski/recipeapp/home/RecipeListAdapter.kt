package com.oski.recipeapp.home

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.oski.recipeapp.R
import com.oski.recipeapp.data.Recipe
import com.oski.recipeapp.helpers.SafeClickListener
import com.oski.recipeapp.recipeForm.RecipeFormActivity


class RecipeListAdapter(
    private val recipesList: List<Recipe>
) : RecyclerView.Adapter<RecipeListAdapter.ViewHolder>() {

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.recipe_card_layout, parent, false)

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return recipesList.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cardName.text = recipesList[position].name

        holder.cardLayout.setSafeOnClickListener {
            val context = it.context
            val intent = Intent(context, RecipeFormActivity::class.java)
            intent.putExtra("recipeData", recipesList[position])

            context.startActivity(intent)
        }

        if (!recipesList[position].imageUrl.isNullOrEmpty()) {
            Glide.with(holder.context)
                .load(recipesList[position].imageUrl)
                .fitCenter()
                .into(holder.cardImage)
        } else {
            Glide.with(holder.context)
                .load(R.drawable.square_placeholder)
                .fitCenter()
                .into(holder.cardImage)
        }
    }

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardLayout: androidx.cardview.widget.CardView = itemView as androidx.cardview.widget.CardView
        val cardName: TextView = itemView.findViewById(R.id.recipe_card_name)
        val cardImage: ImageView = itemView.findViewById(R.id.recipe_card_display_image)
        val context: Context = itemView.context
    }
}