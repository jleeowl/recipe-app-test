package com.oski.recipeapp.home

import com.oski.recipeapp.data.Recipe

class HomePresenter(view: HomeContract.View) : HomeContract.Presenter {

    private var view: HomeContract.View = view
    private var model: HomeContract.Model = HomeModel(view)

    override fun checkFirstTime() {
        val isFirstTime = model.checkFirstTime()

        if (isFirstTime) {
            model.populateDummyData()
        }

        view.showRecipeList(model.getRecipeList())
    }

    override fun getRecipeList(recipeType: String?): ArrayList<Recipe> {
        return model.getRecipeList(recipeType)
    }

    override fun getCurrentRecipeTypeSelection(): String? {
        return model.getCurrentRecipeTypeSelection()
    }

    override fun getRecipeTypes(): MutableList<String>? {
        return model.getRecipeTypes()
    }

    override fun setCurrentRecipeTypeSelection(recipeType: String) {
        model.setCurrentRecipeTypeSelection(recipeType)
    }
}
