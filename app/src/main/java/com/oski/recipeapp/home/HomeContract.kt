package com.oski.recipeapp.home

import com.oski.recipeapp.data.Recipe

interface HomeContract {
    interface View {
        fun initView()
        fun showRecipeList(recipeList: ArrayList<Recipe>)
    }

    interface Presenter {
        fun checkFirstTime()
        fun getRecipeList(recipeType: String? = null): ArrayList<Recipe>
        fun getCurrentRecipeTypeSelection(): String?
        fun getRecipeTypes(): MutableList<String>?
        fun setCurrentRecipeTypeSelection(recipeType: String)
    }

    interface Model {
        fun checkFirstTime(): Boolean
        fun populateDummyData()
        fun getRecipeList(recipeType: String? = null) : ArrayList<Recipe>
        fun getCurrentRecipeTypeSelection() : String?
        fun getRecipeTypes(): MutableList<String>?
        fun setCurrentRecipeTypeSelection(recipeType: String)
    }
}