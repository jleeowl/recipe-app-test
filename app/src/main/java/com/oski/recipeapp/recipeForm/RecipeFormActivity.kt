package com.oski.recipeapp.recipeForm

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.*
import com.oski.recipeapp.data.Recipe
import com.oski.recipeapp.helpers.StoredConstants
import com.oski.recipeapp.helpers.SafeClickListener
import android.database.Cursor
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.oski.recipeapp.R
import java.io.File


class RecipeFormActivity : AppCompatActivity(), RecipeFormContract.View {

    private lateinit var presenter: RecipeFormContract.Presenter

    private lateinit var mRecipeTypeSpinner: Spinner
    private lateinit var mRecipeImage: ImageView
    private lateinit var mRecipeNameEt: EditText
    private lateinit var mRecipePrepTimeEt: EditText
    private lateinit var mRecipeIngredientsEt: EditText
    private lateinit var mRecipePrepStepsEt: EditText
    private lateinit var mRecipeSaveBtn: Button
    private lateinit var mRecipeDelBtn: Button
    private lateinit var mRecipeEditBtn: Button

    private lateinit var mRecipeTypeDisplay: TextView
    private lateinit var mRecipeNameDisplay: TextView
    private lateinit var mRecipePrepTimeDisplay: TextView
    private lateinit var mRecipeIngredientsDisplay: TextView
    private lateinit var mRecipePrepStepsDisplay: TextView

    private lateinit var mControlsLayout: View
    private lateinit var mEditLayout: View

    private var recipeData: Recipe? = null
    private var isImageSelected: Boolean = false
    private var imageUrl: Uri? = null

    private fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
        val safeClickListener = SafeClickListener {
            onSafeClick(it)
        }
        setOnClickListener(safeClickListener)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_form)

        if (intent.hasExtra("recipeData")) {
            recipeData = intent.extras!!.getSerializable("recipeData") as Recipe
        }


        presenter = RecipeFormPresenter(this)

        initView()
    }

    override fun initView() {
        mControlsLayout = findViewById(R.id.recipe_form_controls_layout)
        mControlsLayout.visibility = View.GONE

        mEditLayout = findViewById(R.id.recipe_form_edit_layout)
        mEditLayout.visibility = View.GONE

        if (recipeData == null) {
            mControlsLayout.visibility = View.VISIBLE
        } else {
            mEditLayout.visibility = View.VISIBLE
        }

        mRecipeTypeSpinner = findViewById(R.id.recipe_form_type_spinner)
        val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, presenter.getRecipeTypes())
        mRecipeTypeSpinner.adapter = spinnerAdapter

        mRecipeImage = findViewById(R.id.recipe_form_image)
        mRecipeImage.setSafeOnClickListener {
            // TODO: Add runtime permission for write external access
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showToast("This permission is required to save read and save images in your gallery")

                }

                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    StoredConstants.RequestCodes.REQUEST_WRITE_EXTERNAL_STORAGE)
            } else {
                presenter.pickImageFromGallery()
            }
        }

        mRecipeNameDisplay = findViewById(R.id.recipe_form_display_name)
        mRecipeTypeDisplay = findViewById(R.id.recipe_form_display_type)
        mRecipePrepTimeDisplay = findViewById(R.id.recipe_form_display_prep_time)
        mRecipeIngredientsDisplay = findViewById(R.id.recipe_form_display_ingredients)
        mRecipePrepStepsDisplay = findViewById(R.id.recipe_form_display_prep_steps)

        mRecipeNameEt = findViewById(R.id.recipe_form_name)
        mRecipePrepTimeEt = findViewById(R.id.recipe_form_prep_time)
        mRecipeIngredientsEt = findViewById(R.id.recipe_form_ingredients)
        mRecipePrepStepsEt = findViewById(R.id.recipe_form_prep_steps)

        mRecipeNameDisplay.visibility = View.GONE
        mRecipeTypeDisplay.visibility = View.GONE
        mRecipePrepTimeDisplay.visibility = View.GONE
        mRecipeIngredientsDisplay.visibility = View.GONE
        mRecipePrepStepsDisplay.visibility = View.GONE

        mRecipeNameEt.visibility = View.VISIBLE
        mRecipePrepTimeEt.visibility = View.VISIBLE
        mRecipeIngredientsEt.visibility = View.VISIBLE
        mRecipePrepStepsEt.visibility = View.VISIBLE
        mRecipeImage.isClickable = true
        mRecipeTypeSpinner.visibility = View.VISIBLE

        mRecipeEditBtn = findViewById(R.id.recipe_form_edit)
        mRecipeEditBtn.setSafeOnClickListener {
           if (recipeData != null) {
               mEditLayout.visibility = View.GONE
               mControlsLayout.visibility = View.VISIBLE

               mRecipeNameEt.visibility = View.VISIBLE
               mRecipePrepTimeEt.visibility = View.VISIBLE
               mRecipeIngredientsEt.visibility = View.VISIBLE
               mRecipePrepStepsEt.visibility = View.VISIBLE
               mRecipeImage.isClickable = true
               mRecipeTypeSpinner.visibility = View.VISIBLE

               mRecipeNameDisplay.visibility = View.GONE
               mRecipeTypeDisplay.visibility = View.GONE
               mRecipePrepTimeDisplay.visibility = View.GONE
               mRecipeIngredientsDisplay.visibility = View.GONE
               mRecipePrepStepsDisplay.visibility = View.GONE
           }
        }

        mRecipeSaveBtn = findViewById(R.id.recipe_form_save)
        mRecipeSaveBtn.setSafeOnClickListener {
            if (recipeData == null) {
                mEditLayout.visibility = View.VISIBLE
                mControlsLayout.visibility = View.GONE

                val imageName = getAbsolutePath(imageUrl!!).substring(getAbsolutePath(imageUrl!!).lastIndexOf('/') + 1)
                val targetFile: File? = File(getAbsolutePath(imageUrl!!)).copyTo(File(filesDir, imageName), true)

                presenter.submitForm(Recipe(
                    name = mRecipeNameEt.text.toString(),
                    type = mRecipeTypeSpinner.selectedItem.toString(),
                    imageUrl = if (targetFile != null) targetFile.path else "",
                    preparationTime = mRecipePrepTimeEt.text.toString(),
                    ingredients =  mRecipeIngredientsEt.text.toString(),
                    preparationSteps = mRecipePrepStepsEt.text.toString()
                ))
            } else {
                mControlsLayout.visibility = View.VISIBLE

                var imageName: String?
                var targetFile: File? = null

                if (isImageSelected) {
                    imageName = getAbsolutePath(imageUrl!!).substring(getAbsolutePath(imageUrl!!).lastIndexOf('/') + 1)
                    targetFile = File(getAbsolutePath(imageUrl!!)).copyTo(File(filesDir, imageName), true)
                } else {
                    if (imageUrl != null) {
                        targetFile = File(imageUrl!!.path)
                    }
                }

                presenter.submitForm(Recipe(
                    id = recipeData!!.id,
                    name = mRecipeNameEt.text.toString(),
                    type = mRecipeTypeSpinner.selectedItem.toString(),
                    imageUrl = if (targetFile != null) targetFile.path else "",
                    preparationTime = mRecipePrepTimeEt.text.toString(),
                    ingredients =  mRecipeIngredientsEt.text.toString(),
                    preparationSteps = mRecipePrepStepsEt.text.toString()
                ))
            }

            mEditLayout.visibility = View.VISIBLE
            mControlsLayout.visibility = View.GONE

            mRecipeNameEt.visibility = View.GONE
            mRecipePrepTimeEt.visibility = View.GONE
            mRecipeIngredientsEt.visibility = View.GONE
            mRecipePrepStepsEt.visibility = View.GONE
            mRecipeImage.isClickable = false
            mRecipeTypeSpinner.visibility = View.GONE

            mRecipeNameDisplay.text = mRecipeNameEt.text.toString()
            mRecipeTypeDisplay.text = mRecipeTypeSpinner.selectedItem.toString()
            mRecipePrepTimeDisplay.text = mRecipePrepTimeEt.text.toString()
            mRecipeIngredientsDisplay.text = mRecipeIngredientsEt.text.toString()
            mRecipePrepStepsDisplay.text = mRecipePrepStepsEt.text.toString()

            mRecipeNameDisplay.visibility = View.VISIBLE
            mRecipeTypeDisplay.visibility = View.VISIBLE
            mRecipePrepTimeDisplay.visibility = View.VISIBLE
            mRecipeIngredientsDisplay.visibility = View.VISIBLE
            mRecipePrepStepsDisplay.visibility = View.VISIBLE
        }

        mRecipeDelBtn = findViewById(R.id.recipe_form_delete)

        mRecipeDelBtn.setSafeOnClickListener {
            if (recipeData != null) {
                presenter.discardRecipe(recipeData!!)
            }

            finish()
        }

        recipeData?.let { populateForm(it) }
    }

    override fun populateForm(recipe: Recipe) {
        var index = 0
        for (recipeType in presenter.getRecipeTypes()) {
            if (recipeType == recipeData!!.type) {
                mRecipeTypeSpinner.setSelection(index)
                break
            }
            index++
        }

        mRecipeNameDisplay.visibility = View.VISIBLE
        mRecipeTypeDisplay.visibility = View.VISIBLE
        mRecipePrepTimeDisplay.visibility = View.VISIBLE
        mRecipeIngredientsDisplay.visibility = View.VISIBLE
        mRecipePrepStepsDisplay.visibility = View.VISIBLE

        mRecipeNameEt.visibility = View.GONE
        mRecipePrepTimeEt.visibility = View.GONE
        mRecipeIngredientsEt.visibility = View.GONE
        mRecipePrepStepsEt.visibility = View.GONE
        mRecipeImage.isClickable = false
        mRecipeTypeSpinner.visibility = View.GONE

        mRecipeNameDisplay.text = recipe.name
        mRecipeTypeDisplay.text = recipe.type
        mRecipePrepTimeDisplay.text = recipe.preparationTime
        mRecipeIngredientsDisplay.text = recipe.ingredients
        mRecipePrepStepsDisplay.text = recipe.preparationSteps

        mRecipeNameEt.setText(recipe.name)
        mRecipePrepTimeEt.setText(recipe.preparationTime)
        mRecipeIngredientsEt.setText(recipe.ingredients)
        mRecipePrepStepsEt.setText(recipe.preparationSteps)

        if (!recipe.imageUrl.isNullOrEmpty()) {
            imageUrl = Uri.parse(recipe.imageUrl)

            Glide.with(this)
                .load(recipe.imageUrl)
                .fitCenter()
                .into(mRecipeImage)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                StoredConstants.RequestCodes.PICK_FROM_GALLERY -> {
                    val selectedImage = data?.data

                    imageUrl = selectedImage!!
                    isImageSelected = true

                    Glide.with(this)
                        .load(imageUrl)
                        .fitCenter()
                        .into(mRecipeImage)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            StoredConstants.RequestCodes.REQUEST_WRITE_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    presenter.pickImageFromGallery()
                } else {
                    showToast("Permission denied")
                }
                return
            }
        }
    }

    private fun getAbsolutePath(contentUri: Uri): String {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = contentResolver.query(contentUri, proj, null, null, null)
            val columnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(columnIndex)
        } finally {
            cursor?.close()
        }
    }

    private fun showToast(message: String) {
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(applicationContext, message, duration)
        toast.show()
    }
}
