package com.oski.recipeapp.recipeForm

import android.content.Context
import android.content.Intent
import android.content.res.XmlResourceParser
import com.oski.recipeapp.data.Recipe
import com.oski.recipeapp.repository.DbContract
import com.oski.recipeapp.repository.RecipesDbHelper
import org.xmlpull.v1.XmlPullParser
import android.app.Activity
import com.oski.recipeapp.R
import com.oski.recipeapp.helpers.StoredConstants


class RecipeFormModel (view: RecipeFormContract.View): RecipeFormContract.Model {

    private val context: Context = view as Context
    private val db: DbContract.Recipes = RecipesDbHelper(context)

    private val xrp: XmlResourceParser = context.resources.getXml(R.xml.recipe_types)
    private var recipeTypes: MutableList<String>? = arrayListOf()

    init {
        if (recipeTypes.isNullOrEmpty()) {
            while (xrp.eventType != XmlPullParser.END_DOCUMENT) {
                if (xrp.eventType == XmlPullParser.START_TAG) {
                    if (xrp.name == "type") {
                        recipeTypes?.add(xrp.getAttributeValue(0))
                    }
                }

                xrp.next()
            }
        }
    }

    override fun uploadFormToDatabase(recipe: Recipe) {
        if (recipe.id == null) {
            db.insertRecipe(recipe)
        } else {
            db.updateRecipe(recipe)
        }
    }

    override fun getRecipeTypes(): ArrayList<String> {
        return recipeTypes as ArrayList<String>
    }

    override fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        val mimeTypes = arrayOf("image/jpeg", "image/png")

        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

        (context as Activity).startActivityForResult(intent, StoredConstants.RequestCodes.PICK_FROM_GALLERY)
    }

    override fun discardRecipe(recipeData: Recipe) {
        db.deleteRecipe(recipeData)
    }
}