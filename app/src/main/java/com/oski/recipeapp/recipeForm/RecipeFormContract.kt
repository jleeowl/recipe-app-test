package com.oski.recipeapp.recipeForm

import com.oski.recipeapp.data.Recipe

class RecipeFormContract {
    interface View {
        fun initView()
        fun populateForm(recipe: Recipe)
    }

    interface Presenter {
        fun submitForm(recipe: Recipe)
        fun getRecipeTypes() : ArrayList<String>
        fun pickImageFromGallery()
        fun discardRecipe(recipeData: Recipe)
    }

    interface Model {
        fun uploadFormToDatabase(recipe: Recipe)
        fun getRecipeTypes() : ArrayList<String>
        fun pickImageFromGallery()
        fun discardRecipe(recipeData: Recipe)
    }
}
