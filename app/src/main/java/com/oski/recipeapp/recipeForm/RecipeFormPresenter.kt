package com.oski.recipeapp.recipeForm

import com.oski.recipeapp.data.Recipe

class RecipeFormPresenter(view: RecipeFormContract.View) : RecipeFormContract.Presenter {

    private var view: RecipeFormContract.View = view
    private var model: RecipeFormContract.Model = RecipeFormModel(view)

    override fun submitForm(recipe: Recipe) {
        model.uploadFormToDatabase(recipe)
    }

    override fun getRecipeTypes(): ArrayList<String> {
        return model.getRecipeTypes()
    }

    override fun pickImageFromGallery() {
        model.pickImageFromGallery()
    }

    override fun discardRecipe(recipeData: Recipe) {
        model.discardRecipe(recipeData)
    }
}