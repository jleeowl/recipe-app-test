package com.oski.recipeapp.repository

import com.oski.recipeapp.data.Recipe

class DbContract {
    interface Recipes {
        fun retrieveAllRecipes(recipeType: String? = null): ArrayList<Recipe>
        fun retrieveRecipeById(id: Long): ArrayList<Recipe>
        fun insertRecipe(recipe: Recipe): Long?
        fun updateRecipe(recipe: Recipe): Boolean
        fun deleteRecipe(recipe: Recipe)
    }
}