package com.oski.recipeapp.repository

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.oski.recipeapp.data.Recipe

class RecipesDbHelper(private val context: Context):
    SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION),
    DbContract.Recipes {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    @Throws(SQLiteConstraintException::class)
    override fun retrieveRecipeById(id: Long): ArrayList<Recipe> {
        val recipesList = ArrayList<Recipe>()
        val db = writableDatabase

        val query = "SELECT * FROM ${TableDefinitions.RecipesEntry.TABLE_NAME} WHERE ${TableDefinitions.RecipesEntry.ID} =  $id"
        val cursor = db.rawQuery(query, null)

        if (cursor.moveToFirst()) {
            do {
                recipesList.add(Recipe(
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.ID))),
                    cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.NAME)),
                    cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.TYPE)),
                    cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.IMAGE_URL)),
                    cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.INGREDIENTS)),
                    cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.PREPARATION_STEPS)),
                    cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.PREPARATION_TIME))))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()

        return recipesList
    }

    @Throws(SQLiteConstraintException::class)
    override fun retrieveAllRecipes(recipeType: String?): ArrayList<Recipe> {
        val recipesList = ArrayList<Recipe>()
        val db = writableDatabase


        var query = "SELECT * FROM ${TableDefinitions.RecipesEntry.TABLE_NAME}"
        if (recipeType != null) {
            query += " WHERE ${TableDefinitions.RecipesEntry.TYPE} = '$recipeType'"
        }

        val cursor = db.rawQuery(query, null)

        if (cursor.moveToFirst()) {
            do {
                recipesList.add(Recipe(
                    id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.ID))),
                    name = cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.NAME)),
                    type = cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.TYPE)),
                    imageUrl = cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.IMAGE_URL)),
                    ingredients = cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.INGREDIENTS)),
                    preparationSteps = cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.PREPARATION_STEPS)),
                    preparationTime = cursor.getString(cursor.getColumnIndex(TableDefinitions.RecipesEntry.PREPARATION_TIME))))
            } while (cursor.moveToNext())
        }
        cursor.close()
        db.close()

        return recipesList
    }

    @Throws(SQLiteConstraintException::class)
    override fun insertRecipe(recipe: Recipe): Long? {
        val db = writableDatabase

        val values = ContentValues()
        values.put(TableDefinitions.RecipesEntry.IMAGE_URL, recipe.imageUrl)
        values.put(TableDefinitions.RecipesEntry.INGREDIENTS, recipe.ingredients)
        values.put(TableDefinitions.RecipesEntry.NAME, recipe.name)
        values.put(TableDefinitions.RecipesEntry.PREPARATION_STEPS, recipe.preparationSteps)
        values.put(TableDefinitions.RecipesEntry.PREPARATION_TIME, recipe.preparationTime)
        values.put(TableDefinitions.RecipesEntry.TYPE, recipe.type)

        val id = db.insert(TableDefinitions.RecipesEntry.TABLE_NAME, null, values)
        db.close()

        return id
    }

    @Throws(SQLiteConstraintException::class)
    override fun updateRecipe(recipe: Recipe): Boolean {
        val db = writableDatabase

        val where = "id = ?"
        val whereArgs = arrayOf(recipe.id.toString())

        val values = ContentValues()
        values.put(TableDefinitions.RecipesEntry.IMAGE_URL, recipe.imageUrl)
        values.put(TableDefinitions.RecipesEntry.INGREDIENTS, recipe.ingredients)
        values.put(TableDefinitions.RecipesEntry.NAME, recipe.name)
        values.put(TableDefinitions.RecipesEntry.PREPARATION_STEPS, recipe.preparationSteps)
        values.put(TableDefinitions.RecipesEntry.PREPARATION_TIME, recipe.preparationTime)
        values.put(TableDefinitions.RecipesEntry.TYPE, recipe.type)

        val successful = db.update(TableDefinitions.RecipesEntry.TABLE_NAME, values, where, whereArgs) > 0
        db.close()

        return successful
    }

    @Throws(SQLiteConstraintException::class)
    override fun deleteRecipe(recipe: Recipe) {
        val db = writableDatabase
        db.execSQL("DELETE FROM ${TableDefinitions.RecipesEntry.TABLE_NAME} WHERE ${TableDefinitions.RecipesEntry.ID} = ${recipe.id}")

        db.close()
    }

    companion object {
        const val DB_NAME = "recipe_book.db"
        const val DB_VERSION = 1

        private const val SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TableDefinitions.RecipesEntry.TABLE_NAME + "(" +
                    TableDefinitions.RecipesEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    TableDefinitions.RecipesEntry.NAME + " TEXT, " +
                    TableDefinitions.RecipesEntry.TYPE + " TEXT, " +
                    TableDefinitions.RecipesEntry.IMAGE_URL + " TEXT, " +
                    TableDefinitions.RecipesEntry.INGREDIENTS + " TEXT, " +
                    TableDefinitions.RecipesEntry.PREPARATION_STEPS + " TEXT, " +
                    TableDefinitions.RecipesEntry.PREPARATION_TIME + " TEXT)"

        private const val SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TableDefinitions.RecipesEntry.TABLE_NAME
    }
}