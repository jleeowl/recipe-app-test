package com.oski.recipeapp.repository

import android.provider.BaseColumns

object TableDefinitions {
    class RecipesEntry : BaseColumns {
        companion object {
            const val TABLE_NAME = "recipes"
            const val ID = "id"
            const val NAME = "name"
            const val TYPE = "type"
            const val INGREDIENTS = "ingredients"
            const val IMAGE_URL = "imageUrl"
            const val PREPARATION_TIME = "preparation_time"
            const val PREPARATION_STEPS = "preparation_steps"
        }
    }
}