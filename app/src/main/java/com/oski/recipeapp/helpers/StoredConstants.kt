package com.oski.recipeapp.helpers

object StoredConstants {
    class RequestCodes {
        companion object {
            const val PICK_FROM_GALLERY = 1001
            const val REQUEST_WRITE_EXTERNAL_STORAGE = 3001
        }
    }
}