package com.oski.recipeapp.helpers

import android.content.Context
import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.oski.recipeapp.R

class RecipeListCardDecoration(private val context: Context) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.left = context.resources.getDimensionPixelSize(R.dimen.recipe_list_card_left)
        outRect.right = context.resources.getDimensionPixelSize(R.dimen.recipe_list_card_right)
        outRect.bottom = context.resources.getDimensionPixelSize(R.dimen.recipe_list_card_bottom)
        outRect.top = context.resources.getDimensionPixelSize(R.dimen.recipe_list_card_top)
    }
}