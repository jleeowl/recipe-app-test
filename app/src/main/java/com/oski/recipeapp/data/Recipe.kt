package com.oski.recipeapp.data

import java.io.Serializable

class Recipe(val id: Int? = null,
             val name: String,
             val type: String,
             val imageUrl: String,
             val ingredients: String,
             val preparationTime: String,
             val preparationSteps: String) : Serializable